import DataTable from './data-table/index.js';

import laureates from './laureate.js';
import applicants from './applicants.js';

const tableLaureates = new DataTable(document.body);
tableLaureates.data = laureates.map(laureate => {
    let res = {
        '#': Number(laureate.id),
        'First Name': laureate.firstname,
        'Last Name': laureate.surname,
        'Born Location': [laureate.bornCity, laureate.bornCountry].filter(i => i).join(', '),
        'Died Location': [laureate.diedCity, laureate.diedCountry].filter(i => i).join(', '),
        'Gender': laureate.gender,
        'Prizes': laureate.prizes
            .filter(({year, category}) => year && category)
            .map(({year, category}) => `${category}, ${year}`).join('; ')
    };

    if (laureate.born !== '0000-00-00') {
        res = {
            ...res,
            Born: new Date(Date.parse(laureate.born.replace('-00-00', '-01-01')))
        };
    }

    if (laureate.died !== '0000-00-00') {
        res = {
            ...res,
            Died: new Date(Date.parse(laureate.died.replace('-00-00', '-01-01')))
        };
    }

    return Object.entries(res).reduce((memo, [key, value]) => {
        if (value) {
            return {...memo, [key]: value}
        } else {
            return memo;
        }
    }, {});
});
tableLaureates.build();

const tableApplicants = new DataTable(document.body);
tableApplicants.data = Object.entries(applicants).map(([ID, Name]) => ({
    ID: Number(ID),
    Name
}));
tableApplicants.build();