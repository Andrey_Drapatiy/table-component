export default class DataTable {
  #parent;
  #data;
  #step = 10;
  #stepsValues = [10, 25, 50, 100]
  #paginationStep = 0;
  #sortDirection = 'asc';
  #sortedData;


  constructor(parent) {
    this.#parent = parent;
  }

  set data(data) {
    this.#data = data;
  }

  keys(data) {
    return Object.keys(data[0])
  }

  calcByStep(data) {
    const firstStep = () => this.#paginationStep === 0 ? 0 : this.#paginationStep * this.#step

    return data.slice(firstStep(), this.#paginationStep * this.#step + this.#step)
  }

  calcStepsLength() {
    return Math.ceil(this.#sortedData.length / this.#step)
  }

  clearTable(container) {
    container.querySelector('table').remove()
    container.querySelector('select').remove()
    container.querySelector('nav').remove()
  }

  reRender(container) {
    this.draw(this.calcByStep(this.#sortedData), this.calcStepsLength(), container)
  }

  dataSort(container, sortBy) {
    this.#sortDirection === 'asc' ? this.#sortDirection = 'desc' : this.#sortDirection = 'asc'

    const sortable = this.#sortedData.filter(item => item[sortBy])
    const rest = this.#sortedData.filter(item => !item[sortBy])

    if (typeof sortable[0][sortBy] === "number") {

      const result = this.#sortDirection === 'asc' ?
        sortable.sort((a, b) => (a[sortBy]) - b[sortBy]) :
        sortable.sort((a, b) => (b[sortBy]) - a[sortBy])

      this.#sortedData = [...result, ...rest]

    } else if (typeof sortable[0][sortBy] === "string") {
      const result = this.#sortDirection === 'asc' ?
        sortable.sort((a, b) => (a[sortBy]).toString().localeCompare((b[sortBy]).toString())) :
        sortable.sort((a, b) => (b[sortBy]).toString().localeCompare((a[sortBy]).toString()))

      this.#sortedData = [...result, ...rest]
    }

    this.paginate(0, container)
  }

  paginate(to, container) {
    this.clearTable(container)
    this.#paginationStep = to
    this.reRender(container)
  }

  changePageSize(size, container) {
    this.#step = size
    this.paginate(0, container)
  }

  draw(data, steps, currContainer) {

    const container = currContainer ? currContainer : document.createElement('div')
    container.classList.add('container')

    const cols = this.keys(this.#sortedData)

    const table = document.createElement('table')
    table.classList.add('table')
    const tbody = document.createElement("tbody")

    const thead = document.createElement("thead")
    const row = document.createElement('tr')

    for (let i = 0; i < cols.length; i++) {

      const cell = document.createElement("th")
      const text = document.createTextNode(cols[i])
      cell.appendChild(text)
      cell.addEventListener('click', () => this.dataSort(container, cols[i]))
      row.appendChild(cell)
    }

    thead.appendChild(row)
    table.appendChild(thead)

    for (let i = 0; i < data.length; i++) {

      const row = document.createElement('tr')

      for (let z = 0; z < cols.length; z++) {
        const isExist = data[i][cols[z]] ? data[i][cols[z]] : '—'

        const cell = document.createElement("td")
        const text = document.createTextNode(isExist)

        cell.appendChild(text)
        row.appendChild(cell)
      }

      tbody.appendChild(row)
    }

    table.appendChild(tbody)

    const nav = document.createElement('nav')

    const pagination = document.createElement('ul')
    pagination.classList.add('pagination')
    pagination.classList.add('pagination-sm')

    for (let i = 0; i < steps; i++) {
      const button = document.createElement('li')
      button.classList.add('page-item')

      i === this.#paginationStep ? button.classList.add('active') : null

      button.addEventListener('click', () => this.paginate(i, container))

      const link = document.createElement('a')
      const text = document.createTextNode(i + 1)
      link.appendChild(text)

      button.appendChild(link)
      pagination.appendChild(button)
    }

    const select = document.createElement('select')

    select.addEventListener('change', (event) => this.changePageSize(parseInt(event.target.value), container))

    for (let i = 0; i < this.#stepsValues.length; i++) {
      const option = document.createElement('option')
      const text = document.createTextNode(this.#stepsValues[i])
      option.appendChild(text)
      option.setAttribute('value', this.#stepsValues[i])

      select.appendChild(option)
    }

    select.value = this.#step

    container.appendChild(table)
    container.appendChild(select)
    nav.appendChild(pagination)
    container.appendChild(nav)

    return container
  }

  build() {
    this.#sortedData = this.#data
    this.#parent.appendChild(this.draw(this.calcByStep(this.#sortedData), this.calcStepsLength()))
  }
}
